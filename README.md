# Microservicios con Spring Boot y Spring Cloud Netflix Eureka <a name="inicio"/>

## Contenido
+ 1. [Introducción](#intro)


---
## Introducción <a name="intro"/>[↑](#inicio)

### ¿Qué son los microservicios?
Son un enfoque que nos permite desarrollar aplicaciones que están compuestas en servicios, es decir, en lugar de tener una gran aplicación, un monolito (monolítica), se tienen varios proyectos, varios microservicios independientes que se comunican entre sí mediante **API Rest**. Esto hace que las aplicaciones sean más fáciles de escalar y de desarrollar.
Los servicios son un conjunto de componentes pequeños que son autónomos y colaboran entre sí para llevar a cabo una gran tarea, una aplicación.

### Características.
Algunas características de los microservicios:
* Son **autónomos**. Cada servicio, de forma independiente, tiene su propio equipo de desarrollo, también se puede implementar, desplegar y escalar de forma autónoma sin afectar al resto de los servicios.
* Son **especializados**. Cada servicio tiene una función única dentro de su aplicación.
* Utilizan el **Registro automático** que se le conoce como *descubrimiento de los servicios*. Cuando se despliega o se escala un micro servicio automáticamente se registra la ubicación la IP, el puerto, el nombre del dominio, etc. en un servidor de nombre
* Trabajan con **Escalados flexible**. Los servicios se pueden escalar dependiendo de la demanda. Cuando hay mucha demanda en un servicio determinado automáticamente tiene que aumentar su disponibilidad en la nube, en los servidores.
* Utilizan **balanceo carga** para buscar y seleccionar la mejor instancia disponible.
* Trabajan con **tolerancia a fallos**, y **configuración centralizada** que permite controlar las configuraciones de cada servicio desde un solo servidor de configuración utilizando, por ejemplo, Spring Cloud Config, 
* Permiten **Libertad tecnológica**. En cada micro servicio se puede utilizar la tecnología que más atienda esa tarea en particular.
* Permite tener **agilidad** y **equipos de desarrollo más pequeños**. Cada servicio puede tener su propio equipo de desarrollo y esto a su ves permite que los tiempos de desarrollo sean más cortos.
* Facilitan el **código reutilizable**. Cada micro servicio funciona como un módulo, se pueden utilizar en varios otros servicios comunicándose mediante API REST.
